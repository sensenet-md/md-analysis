package com.tcb.mdAnalysis.util;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathUtil {
	public static Path fromResource(ClassLoader classLoader, String path) throws URISyntaxException{
		return Paths.get(classLoader.getResource(path).toURI());
	}
}
