package com.tcb.mdAnalysis;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import com.tcb.mdAnalysis.cli.Mode;
import com.tcb.mdAnalysis.exceptions.InvalidModeException;

public class Main {

	public static void main(String[] args) throws Exception {
		Mode mode = null;
		try{
			mode = getMode(args);
		}catch(InvalidModeException e){
			System.out.println(e.getMessage());
			printModeUsage();
			printAvailableModes();
			System.exit(1);
		}
		
		mode.run(args);
		
		} 
	
	private static Mode getMode(String[] args) throws IllegalArgumentException{
		String modeArgument = null;
		try{
			modeArgument = args[0];
			Mode mode = Mode.valueOf(modeArgument.toUpperCase());
			return mode;
		} catch(ArrayIndexOutOfBoundsException|IllegalArgumentException e){
			throw new InvalidModeException(String.format("Invalid mode specified: %s", modeArgument));
		}
	}
	
	
	public static void printModeUsage(){
		System.out.println("usage: ${BIN} <mode> <mode-options>");
		System.out.println("to get help regarding a specific mode: ${BIN} <mode> -h");
	}
	
	public static void printAvailableModes(){
		String modes = Stream.of(Mode.values())
				.map(m -> m.toString())
				.reduce((a,b) -> a + "," + b)
				.get();
		System.out.println(String.format("Available modes: %s",modes));
	}
}
