package com.tcb.mdAnalysis.math;

public class PowerOf2 {
	public static int nextPowerOf2(int n){
		requirePositive(n);
		if(isPowerOf2(n)) return n;
		int pos = 0;
		while(n > 0){
			pos++;
			n = n >> 1;
		}
		return (int) Math.pow(2, pos);
	}
	
	public static boolean isPowerOf2(int n){
		requirePositive(n);
		return (n & (n-1)) == 0;
	}
	
	public static void requirePositive(int n){
		if(n <= 0) throw new IllegalArgumentException("n must be larger than 0");
	}
}
