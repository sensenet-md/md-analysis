package com.tcb.mdAnalysis.math.integration;

public interface Integrator {
	public Double integrate();
	public Double integrate(int lowLimit, int highLimit);
}
