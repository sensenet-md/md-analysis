package com.tcb.mdAnalysis.math.integration;

import java.util.List;

public class SimpsonAbsoluteIntegrator extends SimpsonIntegrator {

	public SimpsonAbsoluteIntegrator(List<Double> observations) {
		super(observations);
	}
	
	@Override
	protected double simpson(double p1, double p2, double p3){
		p1 = Math.abs(p1);
		p2 = Math.abs(p2);
		p3 = Math.abs(p3);
		return super.simpson(p1,p2,p3);
	}

}
