package com.tcb.mdAnalysis.math.integration;

import java.util.List;

public class ExponentialDecayIntegrator implements Integrator {
	
	private final double a;
	private double b;

	public ExponentialDecayIntegrator(double a, double b){
		// y = a * exp(-b*x)
		this.a = a;
		this.b = b;
		checkInput();
	}
	
	private void checkInput(){
		if(b <= 0) throw new IllegalArgumentException("b must be larger than 0");
	}

	@Override
	public Double integrate() {
		// Integration from 0 to infinity
		return a / b;
	}

	@Override
	public Double integrate(int lowLimit, int highLimit) {
		throw new UnsupportedOperationException("Not implemented yet");
	}
	
}
