package com.tcb.mdAnalysis.math.integration;

import java.util.List;

public class SimpsonIntegrator implements Integrator {
	private List<Double> observations;
	
	public SimpsonIntegrator(List<Double> observations){
		this.observations = observations;

	}
	
	public Double integrate(){
		return integrate(0,observations.size());
	}
	
	public Double integrate(int lowIndex, int highIndex){
		highIndex = adjustHighIndex(lowIndex,highIndex);
		double sum = 0d;
		for(int i=0;i+2 <= highIndex;i=i+2){
			sum += simpson(observations.get(i),observations.get(i+1),observations.get(i+2));
		}
		return sum;
	}
	
	private int adjustHighIndex(int lowIndex, int highIndex){
		if(observations.subList(lowIndex, highIndex).size() % 2 == 1){
			return highIndex;
		}else{
			return highIndex-1;
		}
	}
	
	protected double simpson(double p1, double p2, double p3){
		return (1./3.) * (p1 + 4*p2 + p3);
	}
}
