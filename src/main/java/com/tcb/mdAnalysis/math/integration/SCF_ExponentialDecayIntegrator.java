package com.tcb.mdAnalysis.math.integration;

import com.tcb.mdAnalysis.math.DoubleUtil;

public class SCF_ExponentialDecayIntegrator implements Integrator {

	private static final Double defaultConvergenceLimit = 0.001;
	private static final Integer defaultMaxSteps = 10000;
	
	private final Integer integrationTime;
	private final Double convergenceLimit;
	private final Integer maxSteps;
	private final Integrator integrator;
	private final Double integrationTimeDouble;

	public SCF_ExponentialDecayIntegrator(Integrator integrator, Integer integrationTime, Integer maxSteps, Double convergenceLimit){
		this.integrator = integrator;
		this.integrationTime = integrationTime;
		this.integrationTimeDouble = (double)integrationTime;
		this.convergenceLimit = convergenceLimit;
		this.maxSteps = maxSteps;
	}
	
	public SCF_ExponentialDecayIntegrator(Integrator integrator, Integer integrationTime){
		this(integrator, integrationTime, defaultMaxSteps, defaultConvergenceLimit);
	}
	
	
	@Override
	public Double integrate() {
		double result = 1;
		for(int i=0;i<maxSteps;i++){
			double F = integrator.integrate(0, integrationTime);
			double newResult = F / (1 - Math.exp(-integrationTimeDouble/result));
			System.out.println(newResult);
			if(DoubleUtil.isClose(newResult, result, convergenceLimit)){
				return newResult;
			} else{
				result = newResult;
			}
		}
		throw new RuntimeException("SCF did not converge");
	}
	


	@Override
	public Double integrate(int lowLimit, int highLimit) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

}
