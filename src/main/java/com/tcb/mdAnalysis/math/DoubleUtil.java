package com.tcb.mdAnalysis.math;

public class DoubleUtil {
	
	public static double product(double a, double b){
		return a*b;
	}
	
	public static boolean isClose(double a, double b, double epsilon){
		return Math.abs(a-b) < epsilon;
	}
}
