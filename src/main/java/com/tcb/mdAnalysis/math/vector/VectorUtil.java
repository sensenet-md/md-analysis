package com.tcb.mdAnalysis.math.vector;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class VectorUtil {
	public static double dotProduct(Vector3D a, Vector3D b){
		return a.dotProduct(b);
	}
}
