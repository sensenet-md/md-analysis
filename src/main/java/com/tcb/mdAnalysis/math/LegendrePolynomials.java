package com.tcb.mdAnalysis.math;

public class LegendrePolynomials {
	public static double legendre1(double x){
		return x;
	}
	
	public static double legendre2(double x){
		return 0.5 * (3*Math.pow(x, 2) - 1);
	}
}
