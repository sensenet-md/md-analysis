package com.tcb.mdAnalysis.exceptions;

public class InvalidModeException extends IllegalArgumentException {
	public InvalidModeException(String message){
		super(message);
	}
}
