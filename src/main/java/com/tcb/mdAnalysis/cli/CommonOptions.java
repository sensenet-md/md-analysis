package com.tcb.mdAnalysis.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PatternOptionBuilder;

public class CommonOptions extends Options {
		
	public CommonOptions(){
		
		this.addOption(
				Option.builder("h")
				.type(String.class)
				.desc("print help and exit")
				.build());
		
		
		this.addOption(
				Option.builder("i")
				.argName("input file")
				.type(String.class)
				.hasArg()
				.desc("Input comma separated .csv file with one header line")
				.required()
				.build()
				);
				
		this.addOption(
				Option.builder("o")
				.argName("output tag")
				.type(String.class)
				.hasArg()
				.desc("output tag")
				.required()
				.build());
		
		this.addOption(
				Option.builder("r")
				.argName("regression limit")
				.type(Double.class)
				.hasArg()
				.desc("regression limit")
				.build());
		
	}
}
