package com.tcb.mdAnalysis.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class XYOptions extends CommonOptions {
	
	public XYOptions(){
		super();

		this.addOption(
				Option.builder("x")
				.argName("X column")
				.type(String.class)
				.hasArg()
				.desc("Input .csv columnName for X axis data")
				.required()
				.build()
				);
		
		this.addOption(
				Option.builder("y")
				.argName("Y column")
				.type(String.class)
				.hasArg()
				.desc("Input .csv columnName for Y axis data")
				.required()
				.build()
				);
		
	}
	
}
