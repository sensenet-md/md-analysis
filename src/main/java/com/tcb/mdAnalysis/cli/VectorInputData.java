package com.tcb.mdAnalysis.cli;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import static java.lang.Double.parseDouble;

public class VectorInputData {
	
	public List<Vector3D> vectors;
	
	public VectorInputData(String inputFilePath, int firstCol) throws IOException{
		this.vectors = new ArrayList<Vector3D>();
		BufferedReader br = new BufferedReader(new FileReader(inputFilePath));
		String line;
		while ((line = br.readLine()) != null) {
			   line = line.trim();
		       if(line.startsWith("#")) continue;
		       String[] fields = line.split("\\s+");
		       double x = parseDouble(fields[firstCol]);
		       double y = parseDouble(fields[firstCol+1]);
		       double z = parseDouble(fields[firstCol+2]);
		       
		       Vector3D vector = new Vector3D(x,y,z);
		       	
		       vectors.add(vector);      
		    }
		br.close();
	}
}
