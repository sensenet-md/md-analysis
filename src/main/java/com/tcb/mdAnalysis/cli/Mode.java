package com.tcb.mdAnalysis.cli;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.tcb.mdAnalysis.cli.autocorrelation.FluorescenceAnisotropyMain;
import com.tcb.mdAnalysis.cli.autocorrelation.ScalarAutocorrelationMain;
import com.tcb.mdAnalysis.cli.autocorrelation.VectorAutocorrelationMain;
import com.tcb.mdAnalysis.cli.blockAverage.BlockAverageMain;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;

public enum Mode {
	AUTOCORRELATION,VECTOR_AUTOCORRELATION, BLOCK_AVERAGE, FLUORESCENCE_ANISOTROPY;
	
	public void run(String[] args) throws ParseException,IOException{
		switch(this){
		case AUTOCORRELATION:
			new ScalarAutocorrelationMain().run(args);
			break;
		case BLOCK_AVERAGE:
			new BlockAverageMain().run(args);
			break;
		case VECTOR_AUTOCORRELATION:
			new VectorAutocorrelationMain().run(args);
			break;
		case FLUORESCENCE_ANISOTROPY:
			new FluorescenceAnisotropyMain().run(args);
			break;		
		default:
			throw new IllegalArgumentException("Unknown run mode");
		}
	}
	
	@Override
	public String toString(){
		return this.name().toLowerCase();
	}
			
}
