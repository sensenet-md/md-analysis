package com.tcb.mdAnalysis.cli;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;

public class XYInputData {
	
	public List<Double> X;
	public List<Double> Y;
	
	public XYInputData(String inputFilePath, String xColumnName, String yColumnName) throws IOException{
		CSV csv = new CSV_Reader(inputFilePath,",",true).getCSV();
		this.X = csv.getColumnByName(xColumnName).stream()
				.map(s -> Double.valueOf(s))
				.collect(Collectors.toList());
		this.Y = csv.getColumnByName(yColumnName).stream()
				.map(s -> Double.valueOf(s))
				.collect(Collectors.toList());
	}
}
