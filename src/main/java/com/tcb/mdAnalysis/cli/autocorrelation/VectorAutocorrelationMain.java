package com.tcb.mdAnalysis.cli.autocorrelation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.mdAnalysis.ModeMain;
import com.tcb.mdAnalysis.cli.Mode;
import com.tcb.mdAnalysis.cli.VectorInputData;
import com.tcb.mdAnalysis.cli.VectorOptions;
import com.tcb.mdAnalysis.cli.XYInputData;
import com.tcb.mdAnalysis.cli.XYOptions;
import com.tcb.mdAnalysis.math.LegendrePolynomials;
import com.tcb.mdAnalysis.math.vector.VectorUtil;
import com.tcb.mdAnalysis.plot.AutocorrelationPlot;
import com.tcb.mdAnalysis.plot.Plot;
import com.tcb.mdAnalysis.plot.XYPlot;
import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.DirectAutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationAnalysis;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.common.util.Rounder;
import com.tcb.common.util.Tuple;

public class VectorAutocorrelationMain extends ModeMain {
	protected static final double defaultRegressionLimit = 0.1;
	
	public void run(String[] args) throws ParseException, IOException{
		Options options = new VectorOptions();
		CommandLine cmd = parseArguments(options, args, getMode().toString());
		String inputFile = cmd.getOptionValue('i');
		String outputTag = cmd.getOptionValue('o');
		Optional<String> regressionLimitInput = Optional.ofNullable(cmd.getOptionValue('r'));
		Optional<Double> regressionLimit = Optional.empty();
		if(regressionLimitInput.isPresent()){
			regressionLimit = Optional.of(Double.valueOf(regressionLimitInput.get()));
		}

		
		Integer startingColumn = Integer.parseInt(cmd.getOptionValue('c'));
		VectorInputData inputData = new VectorInputData(inputFile,startingColumn);
		List<Vector3D> observations = inputData.vectors;
		List<Double> autocorrelations = getAutocorrelations(observations);
		AutocorrelationAnalysis analysis = getAutocorrelationAnalysis(autocorrelations, regressionLimit);
		
		Double autocorrelationTime = analysis.getAutocorrelationTime();
		System.out.println(String.format(
				"File,"
				+ "Autocorrelation time"
		));
		System.out.println(String.format("%s,%.4f",
				inputFile,autocorrelationTime));
		
		writeAutocorrelationsToFile(outputTag + ".autocorrelations.csv", autocorrelations);	
		
		Plot plot = new AutocorrelationPlot(analysis);
		plot.plot();
		plot.savePDF(outputTag + ".pdf");
				
	}
	
	protected List<Double> getAutocorrelations(List<Vector3D> vectors){
		return new VectorAutocorrelation(vectors).getAutocorrelations();
	}
	
	protected AutocorrelationAnalysis getAutocorrelationAnalysis(List<Double> autocorrelations, Optional<Double> regressionLimitInput){
		return new AutocorrelationAnalysis(autocorrelations, regressionLimitInput.orElse(defaultRegressionLimit));
	}
	
	@Override
	protected Mode getMode() {
		return Mode.VECTOR_AUTOCORRELATION;
	}
		
	private static void writeAutocorrelationsToFile(String path, List<Double> autocorrelations) throws IOException{
		List<String> lines = new ArrayList<String>();
		lines.add("#k,C");
		for(int i=0; i<autocorrelations.size();i++){
			lines.add(String.format("%d,%.4f", i, autocorrelations.get(i)));
		}
		String content = String.join("\n", lines);
		Files.write(Paths.get(path), content.getBytes());
	}

	
			
}
