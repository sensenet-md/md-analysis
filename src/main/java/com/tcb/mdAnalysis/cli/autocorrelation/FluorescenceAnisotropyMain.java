package com.tcb.mdAnalysis.cli.autocorrelation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.cli.Mode;
import com.tcb.mdAnalysis.math.LegendrePolynomials;
import com.tcb.mdAnalysis.math.vector.VectorUtil;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationAnalysis;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.DirectAutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.fluorescenceAnisotropy.FluorescenceAnisotropy;

public class FluorescenceAnisotropyMain extends VectorAutocorrelationMain {

	
	protected static final double defaultRegressionLimit = 0.1;
	
	@Override
	protected List<Double> getAutocorrelations(List<Vector3D> vectors){
		Autocorrelation fluorescenceAnisotropy = new FluorescenceAnisotropy(vectors,Autocorrelation.getDefaultMaxOffset(vectors));
		List<Double> autocorrelations = fluorescenceAnisotropy.getAutocorrelations();
		return autocorrelations;
	}
	
	@Override
	protected AutocorrelationAnalysis getAutocorrelationAnalysis(List<Double> autocorrelations, Optional<Double> regressionLimit){
		return new AutocorrelationAnalysis(autocorrelations, regressionLimit.orElse(defaultRegressionLimit));
	}
	
	@Override
	protected Mode getMode() {
		return Mode.FLUORESCENCE_ANISOTROPY;
	}
}
