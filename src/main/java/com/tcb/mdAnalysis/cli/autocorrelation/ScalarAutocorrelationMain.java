package com.tcb.mdAnalysis.cli.autocorrelation;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.mdAnalysis.ModeMain;
import com.tcb.mdAnalysis.cli.Mode;
import com.tcb.mdAnalysis.cli.XYInputData;
import com.tcb.mdAnalysis.cli.XYOptions;
import com.tcb.mdAnalysis.plot.AutocorrelationPlot;
import com.tcb.mdAnalysis.plot.Plot;
import com.tcb.mdAnalysis.plot.XYPlot;
import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationAnalysis;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.common.util.Tuple;

public class ScalarAutocorrelationMain extends ModeMain {
	
	protected static final double defaultRegressionLimit = 0.1;
	
	public void run(String[] args) throws ParseException, IOException{
		Options options = new XYOptions();
		CommandLine cmd = parseArguments(options, args, getMode().toString());
		String inputFile = cmd.getOptionValue('i');
		String outputTag = cmd.getOptionValue('o');
		String xColumnName = cmd.getOptionValue('x');
		String yColumnName = cmd.getOptionValue('y');
		
		Optional<String> regressionLimitInput = Optional.ofNullable(cmd.getOptionValue('r'));
		Optional<Double> regressionLimit = Optional.empty();
		if(regressionLimitInput.isPresent()){
			regressionLimit = Optional.of(Double.valueOf(regressionLimitInput.get()));
		}
		
		XYInputData inputData = new XYInputData(inputFile,xColumnName,yColumnName);
		List<Double> observations = inputData.Y;
		Autocorrelation autocorrelation = new ScalarAutocorrelation(observations);
		AutocorrelationAnalysis analysis = new AutocorrelationAnalysis(
				autocorrelation.getAutocorrelations(),
				regressionLimit.orElse(defaultRegressionLimit));
		StandardStatistics stat = new StandardStatistics(observations);
		Double avg = stat.getMean();
		Double stdDev = stat.getStandardDeviation();
		Double effectiveSampleSize = analysis.getEffectiveSampleSize(autocorrelation.getInputDataSize());
		Double autocorrelationTime = analysis.getAutocorrelationTime();
		Double standardError = stat.standardError(effectiveSampleSize);
		System.out.println(String.format(
				"File,"
				+ "Mean,"
				+ "Std. dev,"
				+ "Autocorrelation time,"
				+ "Eff. sample size,"
				+ "Eff.Std.err"));
		System.out.println(String.format("%s,%.4f,%.4f,%.4f,%.4f,%.4f",
				inputFile,avg,stdDev,autocorrelationTime,effectiveSampleSize,standardError));
		Plot plot = new AutocorrelationPlot(analysis);
		plot.plot();
		plot.savePDF(outputTag + ".pdf");
		
		Plot rawDataPlot = new XYPlot(observations);
		rawDataPlot.plot();
		rawDataPlot.savePDF(outputTag + "_rawData.pdf");
		
	}

	@Override
	protected Mode getMode() {
		return Mode.AUTOCORRELATION;
	}
			
}
