package com.tcb.mdAnalysis.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class VectorOptions extends CommonOptions {
	
	public VectorOptions(){
		super();

		this.addOption(
				Option.builder("c")
				.argName("start column")
				.type(Integer.class)
				.hasArg()
				.desc("Start column in whitespace-separated vectors file. A total of three columns beginning"
						+ " from the starting column is read as (x,y,z).")
				.required()
				.build()
				);
		
		
	}
	
}
