package com.tcb.mdAnalysis.cli.blockAverage;

import java.io.IOException;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.tcb.mdAnalysis.ModeMain;
import com.tcb.mdAnalysis.cli.Mode;
import com.tcb.mdAnalysis.cli.XYInputData;
import com.tcb.mdAnalysis.cli.XYOptions;
import com.tcb.mdAnalysis.plot.BlockAveragePlot;
import com.tcb.mdAnalysis.plot.Plot;
import com.tcb.mdAnalysis.statistics.blockAverage.BlockAverage;

public class BlockAverageMain extends ModeMain {
	public void run(String[] args) throws ParseException, IOException {
		Options options = new XYOptions();
		CommandLine cmd = parseArguments(options, args, getMode().toString());
		String inputFile = cmd.getOptionValue('i');
		String outputTag = cmd.getOptionValue('o');
		String xColumnName = cmd.getOptionValue('x');
		String yColumnName = cmd.getOptionValue('y');
		XYInputData inputData = new XYInputData(inputFile,xColumnName,yColumnName);
		List<Double> observations = inputData.Y;
		BlockAverage blockAverage = new BlockAverage(observations);
		
		Plot plot = new BlockAveragePlot(blockAverage);
		plot.plot();
		plot.savePDF(outputTag + ".pdf");
	}

	@Override
	protected Mode getMode() {
		return Mode.BLOCK_AVERAGE;
	}
}
