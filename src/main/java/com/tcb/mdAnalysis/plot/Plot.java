package com.tcb.mdAnalysis.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public abstract class Plot {
	
	public abstract String getName();
	public abstract String getXLabel();
	public abstract String getYLabel();
	public abstract void plot();
	
	private XYPlot plot;
	private XYSeriesCollection dataset;
	private JFreeChart chart;
	
	public Plot(){
		init();
	}
	
	protected void init(){
		this.dataset = new XYSeriesCollection();
		this.chart = ChartFactory.createXYLineChart(
				getName(),
				getXLabel(),
				getYLabel(),
				dataset,
				PlotOrientation.VERTICAL, true, false, false);
		this.plot = (XYPlot)chart.getPlot();
		this.plot.setBackgroundPaint(Color.WHITE);
	}
	
	protected void plotValues(List<Double> values, String name) {
		XYSeries points = new XYSeries(name);
		for(int i=0;i<values.size();i++){
			points.add((double) i, values.get(i));
		}
		dataset.addSeries(points);
	}
	
	protected void addVline(Double x){
		ValueMarker marker = new ValueMarker(x);
		marker.setPaint(Color.black);
		plot.addDomainMarker(marker);
	}
	
	protected void addHline(Double y){
		ValueMarker marker = new ValueMarker(y);
		marker.setPaint(Color.black);
		plot.addRangeMarker(marker);
	}
	
	public void savePDF(String fileName) {
		float offset = 30f;
	    float width = PageSize.A4.rotate().getWidth() - offset;
	    float height = PageSize.A4.rotate().getHeight() - offset;
		Document document = new Document(PageSize.A4.rotate());
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();
			PdfContentByte cb = writer.getDirectContent();
		    PdfTemplate template = cb.createTemplate(width, height);
		    Graphics2D g2d1 = new PdfGraphics2D(template, width, height);
		    Rectangle2D r2d1 = new Rectangle2D.Double(0, 0, width, height);
		    this.chart.draw(g2d1, r2d1);
			g2d1.dispose();
			cb.addTemplate(template, offset/2, offset/2);
		} catch (FileNotFoundException|DocumentException e) {
			e.printStackTrace();
		} finally{
			document.close();
		}
		
	}
	
}
