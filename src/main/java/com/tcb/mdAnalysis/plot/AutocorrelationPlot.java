package com.tcb.mdAnalysis.plot;

import java.util.List;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationAnalysis;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.regression.Regression;

public class AutocorrelationPlot extends Plot {

	private AutocorrelationAnalysis analysis;

	public AutocorrelationPlot(AutocorrelationAnalysis autocorrelation){
		super();
		this.analysis = autocorrelation;
	}
	
	@Override
	public String getName() {
		return "Autocorrelation";
	}

	@Override
	public String getXLabel() {
		return "k";
	}

	@Override
	public String getYLabel() {
		return "C(k)";
	}

	@Override
	public void plot() {
		List<Double> observations = analysis.getAutocorrelations();
		this.plotValues(observations, "Autocorrelation");
		this.plotValues(analysis.getRegression().regressionLine(0, observations.size()), "Exp. fit");
		this.addVline(analysis.getAutocorrelationTime());
	}
	
}

