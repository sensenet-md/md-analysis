package com.tcb.mdAnalysis.plot;

import java.util.List;


public class XYPlot extends Plot {

	private List<Double> observations;

	public XYPlot(List<Double> observations){
		super();
		this.observations = observations;
	}
	
	@Override
	public String getName() {
		return "Input Data";
	}

	@Override
	public String getXLabel() {
		return "X";
	}

	@Override
	public String getYLabel() {
		return "Y";
	}

	@Override
	public void plot() {
		this.plotValues(observations, "Y");
	}
}
