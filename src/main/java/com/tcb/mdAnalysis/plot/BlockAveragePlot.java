package com.tcb.mdAnalysis.plot;

import java.util.List;

import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.blockAverage.BlockAverage;
import com.tcb.mdAnalysis.statistics.regression.Regression;

public class BlockAveragePlot extends Plot {

	private BlockAverage blockAverage;

	public BlockAveragePlot(BlockAverage blockAverage){
		super();
		this.blockAverage = blockAverage;
	}
	
	@Override
	public String getName() {
		return "Std. Blocked Errors";
	}

	@Override
	public String getXLabel() {
		return "Block size";
	}

	@Override
	public String getYLabel() {
		return "Std. Blocked Error";
	}

	@Override
	public void plot() {
		List<Double> observations = blockAverage.getBlockedStandardErrors();
		this.plotValues(observations, "Std. Blocked Error");
	}
	
}

