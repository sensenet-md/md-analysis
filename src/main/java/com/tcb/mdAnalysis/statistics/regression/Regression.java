package com.tcb.mdAnalysis.statistics.regression;

import java.util.List;

public interface Regression {
	public Double getA();
	public Double getB();
	public Double predict(Double x);
	public List<Double> regressionLine(Integer start, Integer end);
	public List<Double> getRoots(Double offset);
}
