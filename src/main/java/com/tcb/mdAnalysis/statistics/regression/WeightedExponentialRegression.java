package com.tcb.mdAnalysis.statistics.regression;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WeightedExponentialRegression extends WeightedLinearRegression {
			
	public WeightedExponentialRegression(List<Double> observations, List<Double> weights) {
		super(log(observations), weights);
	}
	
	private static List<Double> log(List<Double> doubles){
		return doubles.stream().map(d -> Math.log(d)).collect(Collectors.toList());
	}
	
	public WeightedExponentialRegression(List<Double> observations){
		this(observations, Collections.nCopies(observations.size(), 1.0));
	}

	@Override
	public Double getA(){
		return Math.exp(super.getA());
	}
	
	@Override
	public Double getB(){
		return super.getB();
	}
	
	@Override
	public Double predict(Double x){
		return getA() * Math.exp(getB() * x);
	}
	
	@Override
	public List<Double> getRoots(Double offset) {
		return Arrays.asList((Math.log(offset) - super.getA()) / getB());
	}

}
