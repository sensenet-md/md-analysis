package com.tcb.mdAnalysis.statistics.regression;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.correlation.Covariance;
import com.tcb.mdAnalysis.statistics.StandardStatistics;

public class WeightedLinearRegression implements Regression {
	private List<Double> observations;
	private double b;
	private double a;
	private List<Double> weights;

	public WeightedLinearRegression(List<Double> observations, List<Double> weights){
		this.observations = observations;
		this.weights = weights;
		init();
	}
	
	public WeightedLinearRegression(List<Double> observations){
		this(observations, Collections.nCopies(observations.size(), 1.0));
	}
	
	private void init(){
		List<Double> X = IntStream.range(0, observations.size()).
				asDoubleStream().boxed().collect(Collectors.toList());
		final Double xMean = weightedMean(X);
		final Double yMean = weightedMean(observations);
		Double numerator = 0.;
		Double denominator = 0.;
		for(int i=0;i<observations.size();i++){
			double x = X.get(i);
			double y = observations.get(i);
			double w = weights.get(i);
			numerator += w * (x * y - xMean * yMean);
			denominator += w * (x * x - xMean * xMean);
		}
		this.b = numerator / denominator;
		this.a = yMean - b * xMean;	
	}
	
	private Double weightedMean(List<Double> values){
		Double wSum = new StandardStatistics(weights).getSum();
		Double numerator = 0.;
		for(int i=0;i<values.size();i++){
			numerator += values.get(i) * weights.get(i);
		}
		return numerator / wSum;
	}

	@Override
	public Double getA() {
		return a;
	}

	@Override
	public Double getB() {
		return b;
	}

	@Override
	public Double predict(Double x) {
		return a + b * x;
	}

	@Override
	public List<Double> regressionLine(Integer start, Integer end) {
		return IntStream.range(start, end).asDoubleStream().boxed()
				.map(d -> predict(d))
				.collect(Collectors.toList());
	}
	
	@Override
	public List<Double> getRoots(Double offset) {
		return Arrays.asList((offset - getA()) / getB());
	}
	
	
	
}
