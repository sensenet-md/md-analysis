package com.tcb.mdAnalysis.statistics.lifetime;

import java.util.List;

import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;



public class Lifetime extends ScalarAutocorrelation {

	public Lifetime(List<Double> observations, int maxOffset, AutocorrelationStrategy<Double> strategy){
		super(observations, maxOffset, strategy);
	}
	
	public Lifetime(List<Double> observations, int maxOffset){
		super(observations, maxOffset);
	}
	
	public Lifetime(List<Double> observations) {
		super(observations);
	}

	@Override
	protected void prepareData(Double[] data){
		// Do nothing
		return;
	}
}
