package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.tcb.mdAnalysis.math.integration.ExponentialDecayIntegrator;
import com.tcb.mdAnalysis.math.integration.Integrator;
import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedExponentialRegression;

public class AutocorrelationAnalysis {
	
	private Regression regression;
	private List<Double> autocorrelations;

	
	public AutocorrelationAnalysis(List<Double> autocorrelations, Double regressionLimit){
		this.autocorrelations = autocorrelations;
		this.regression = calculateRegression(autocorrelations, regressionLimit);
	}
		
	public Regression getRegression(){
		return regression;
	}
	
	public List<Double> getAutocorrelations(){
		return autocorrelations;
	}
		
	private Regression calculateRegression(List<Double> autocorrelations, Double regressionLimit){
		List<Double> filteredAutocorrelations = autocorrelations.stream()
				.filter(d -> d > regressionLimit)
				.collect(Collectors.toList());
		List<Double> weights = IntStream.rangeClosed(1, filteredAutocorrelations.size()).asDoubleStream()
				.boxed()
				.map(d -> 1/d)
				.collect(Collectors.toList());
		return new WeightedExponentialRegression(filteredAutocorrelations,weights);
	}
	
	public Double getAutocorrelationTime(){
		Integrator integrator = new ExponentialDecayIntegrator(regression.getA(),-regression.getB());
		Double integrated = integrator.integrate();
		Double result = Math.max(integrated,1.0);
		return result;
	}
			
	public Double getEffectiveSampleSize(Integer sampleSize){
		return ((double) sampleSize) / getAutocorrelationTime();
	}
	
	
}
