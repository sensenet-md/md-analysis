package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.List;

public interface Autocorrelation {

	public List<Double> getAutocorrelations();
	public Integer getInputDataSize();
	
	public static int getDefaultMaxOffset(List<?> observations){
		int maxOffset = (int) (0.1 * observations.size());
		return maxOffset - 1;
	}

}