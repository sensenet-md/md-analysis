package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.math.FFT_Adapter;
import com.tcb.mdAnalysis.math.PowerOf2;
import com.tcb.mdAnalysis.statistics.StandardStatistics;

import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorImpl;

public class FFT_AutocorrelationStrategy extends AbstractAutocorrelationStrategy<Double> {
	
	@Override
	public double[] getAutocorrelations(Double[] observations){
		int inputLength = observations.length;
		int workArrayLength = PowerOf2.nextPowerOf2(2 * inputLength);
		// Array initialization and zero padding
		double[] re = new double[workArrayLength];
		double[] im = new double[workArrayLength];
		fillWorkArray(re, observations);
		FFT_Adapter.transformRadix2(re,im);
		squareComplexArray(re,im);
		FFT_Adapter.inverseTransformRadix2(re,im);
		normalize(re);
		return re;
	}
			
	private void fillWorkArray(double[] workArray, Double[] input){
		for(int i=0;i<input.length;i++){
			workArray[i] = input[i];
		}
	}
		
	private void squareComplexArray(double[] re, double[] im){
		for(int i=0;i<re.length;i++){
			// (a + bi) * (a-bi) = a**2 + b**2 
			re[i] = Math.pow(re[i],2) + Math.pow(im[i],2);
			im[i] = 0d;
		}
	}
}
