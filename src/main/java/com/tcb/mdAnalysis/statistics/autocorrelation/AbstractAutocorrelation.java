package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.statistics.StandardStatistics;

public abstract class AbstractAutocorrelation<T> implements Autocorrelation {

	protected abstract double[] calculateAutocorrelations();
	
	protected final int inputSize;
	protected final int maxOffset;
	protected List<T> observations;
	protected AutocorrelationStrategy<T> strategy;
	
	private Optional<List<Double>> autocorrelations;
			
	public AbstractAutocorrelation(List<T> observations, int maxOffset, AutocorrelationStrategy<T> strategy){
		this.inputSize = observations.size();
		this.observations = observations;
		this.autocorrelations = Optional.empty();
		this.strategy = strategy;
		this.maxOffset = maxOffset;
	}
				
	@Override
	public List<Double> getAutocorrelations() {
		if(!autocorrelations.isPresent()){
			double[] rawAutocorrelations = calculateAutocorrelations();
			List<Double> filteredAutocorrelations = filter(rawAutocorrelations);
			autocorrelations = Optional.of(filteredAutocorrelations);
		}
		return autocorrelations.get();
	}
	
	
	
	private List<Double> filter(double[] autocorrelations){
		return Arrays.stream(autocorrelations)
				.limit(maxOffset+1)
				.boxed()
				.collect(Collectors.toList());
	}

	@Override
	public Integer getInputDataSize() {
		return inputSize;
	}
		
}
