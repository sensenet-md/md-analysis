package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.math.LegendrePolynomials;
import com.tcb.mdAnalysis.statistics.StandardStatistics;

public class DirectAutocorrelationStrategy<T> extends AbstractAutocorrelationStrategy<T> {

	protected final BiFunction<T, T, Double> multiplier;
	protected final Function<Double, Double> scalingFun;
	protected final int maxOffset;
	
	public DirectAutocorrelationStrategy(int maxOffset, BiFunction<T,T,Double> multiplier, Function<Double,Double> scalingFun){
		this.maxOffset = maxOffset;
		this.multiplier = multiplier;
		this.scalingFun = scalingFun;
	}
	
	public DirectAutocorrelationStrategy(BiFunction<T,T,Double> multiplier, Function<Double,Double> scalingFun){
		this(Integer.MAX_VALUE-1, multiplier, scalingFun);
	}
	
	public DirectAutocorrelationStrategy(BiFunction<T,T,Double> multiplier){
		this(multiplier, LegendrePolynomials::legendre1);
	}
	
	protected double getAutocorrelation(T[] observations, int h) {
		double sum = 0d;
		final int size = observations.length;
		final int max_i = size - h;
		for(int i=0;i<max_i;i++){
			double product = multiplier.apply(observations[i],observations[i+h]);
			sum += scalingFun.apply(product); 
		}
		double avg = sum / (size - 1);
		return avg;
	}
		
	@Override
	public double[] getAutocorrelations(T[] observations) {
		int maxDataOffset = Math.min(maxOffset, observations.length-1);

		double[] autocorrelations = IntStream.rangeClosed(0, maxDataOffset)
				.parallel()
				.mapToDouble(i -> getAutocorrelation(observations,i))
				.toArray();
		
		normalize(autocorrelations);
				
		return autocorrelations;
	}
}
