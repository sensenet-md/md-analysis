package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

public class UnbiasedEstimateDirectAutocorrelationStrategy<T> extends DirectAutocorrelationStrategy<T> {

	public UnbiasedEstimateDirectAutocorrelationStrategy(int maxOffset, BiFunction<T,T,Double> multiplier,
			Function<Double,Double> scalingFun){
		super(maxOffset, multiplier, scalingFun);
	}
	
	public UnbiasedEstimateDirectAutocorrelationStrategy(BiFunction<T,T,Double> multiplier, Function<Double,Double> scalingFun){
		super(multiplier, scalingFun);
	}
	
	public UnbiasedEstimateDirectAutocorrelationStrategy(BiFunction<T,T,Double> multiplier){
		super(multiplier);
	}
	
	@Override
	protected double getAutocorrelation(T[] observations, int h) {
		final int size = observations.length;
		double rawAutocorrelation = super.getAutocorrelation(observations,h);
		double autocorrelationSum = rawAutocorrelation * (size - 1);
		double autocorrelation = autocorrelationSum / (size - h);
		return autocorrelation;
	}
	
	


}
