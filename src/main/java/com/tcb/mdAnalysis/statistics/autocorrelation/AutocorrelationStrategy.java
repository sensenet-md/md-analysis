package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.List;

public interface AutocorrelationStrategy<T> {
	public double[] getAutocorrelations(T[] observations);
}
