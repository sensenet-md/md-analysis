package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import com.tcb.mdAnalysis.math.DoubleUtil;
import com.tcb.mdAnalysis.math.integration.ExponentialDecayIntegrator;
import com.tcb.mdAnalysis.math.integration.Integrator;
import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedExponentialRegression;

import java.lang.Math;

public class ScalarAutocorrelation extends AbstractAutocorrelation<Double> {
	// Calculate autocorrelation of a set of observations. Largely emulates R's acf() function.
	//final int max_h = Math.min(size-1, (int) ((10*Math.log10(size))));
	
	private static final AutocorrelationStrategy<Double> defaultStrategy = new FFT_AutocorrelationStrategy();
	
	
	public ScalarAutocorrelation(List<Double> observations, int maxOffset, AutocorrelationStrategy<Double> strategy) {
		super(observations, maxOffset, strategy);
	}
	
	public ScalarAutocorrelation(List<Double> observations, int maxOffset){
		this(observations, maxOffset, defaultStrategy);
	}
	
	public ScalarAutocorrelation(List<Double> observations){
		this(observations, Autocorrelation.getDefaultMaxOffset(observations), defaultStrategy);
	}
				
	@Override
	protected double[] calculateAutocorrelations(){
		Double[] workData = new Double[observations.size()];
		workData = observations.toArray(workData);
		prepareData(workData);
		double[] autocorrelations = strategy
				.getAutocorrelations(workData);
		return autocorrelations;
	}
		
	protected void prepareData(Double[] data){
		StandardStatistics stat = new StandardStatistics(data);
		double mean = stat.getMean(); 
		for(int i=0;i<data.length;i++){
			data[i] -= mean;
		}
	}
	
	
		
}
