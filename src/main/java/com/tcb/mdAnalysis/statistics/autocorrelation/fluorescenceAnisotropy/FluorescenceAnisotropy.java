package com.tcb.mdAnalysis.statistics.autocorrelation.fluorescenceAnisotropy;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.math.LegendrePolynomials;
import com.tcb.mdAnalysis.math.vector.VectorUtil;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.DirectAutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;

public class FluorescenceAnisotropy implements Autocorrelation {

	
	protected static final double autocorrelationScaling = 0.4;
	private static final AutocorrelationStrategy<Vector3D> strategy = new DirectAutocorrelationStrategy<Vector3D>(
			VectorUtil::dotProduct,
			LegendrePolynomials::legendre2);
	
	private VectorAutocorrelation autocorrelation;
	
	public FluorescenceAnisotropy(List<Vector3D> orientations, int maxOffset, AutocorrelationStrategy<Vector3D> strategy){
		List<Vector3D> normalizedOrientations = orientations.stream()
				.map(v -> v.normalize())
				.collect(Collectors.toList());
		this.autocorrelation = new VectorAutocorrelation(normalizedOrientations,
				maxOffset, strategy);
	}

	public FluorescenceAnisotropy(List<Vector3D> orientations, int maxOffset){
		this(orientations, maxOffset, strategy);
		
	}
		
	@Override
	public List<Double> getAutocorrelations() {
		List<Double> autocorrelations = autocorrelation.getAutocorrelations();
		List<Double> scaledAutocorrelations = autocorrelations.stream()
				.map(d -> autocorrelationScaling * d)
				.collect(Collectors.toList());
		return scaledAutocorrelations;
	}

	@Override
	public Integer getInputDataSize() {
		return autocorrelation.getInputDataSize();
	}

}
