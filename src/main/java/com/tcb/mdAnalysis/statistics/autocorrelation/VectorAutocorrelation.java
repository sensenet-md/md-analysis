package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import com.tcb.mdAnalysis.math.integration.ExponentialDecayIntegrator;
import com.tcb.mdAnalysis.math.integration.Integrator;
import com.tcb.mdAnalysis.math.vector.VectorUtil;
import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedExponentialRegression;

import java.lang.Math;

public class VectorAutocorrelation extends AbstractAutocorrelation<Vector3D> {

	private static final AutocorrelationStrategy<Vector3D> defaultStrategy = 
			new DirectAutocorrelationStrategy<Vector3D>(VectorUtil::dotProduct);
	
	public VectorAutocorrelation(List<Vector3D> observations, int maxOffset, AutocorrelationStrategy<Vector3D> strategy){
		super(observations, maxOffset, strategy);
	}
	
	public VectorAutocorrelation(List<Vector3D> observations, int maxOffset){
		this(observations, maxOffset, defaultStrategy);
	}
	
	public VectorAutocorrelation(List<Vector3D> observations){
		this(observations, Autocorrelation.getDefaultMaxOffset(observations), defaultStrategy);
	}
						
	@Override
	protected double[] calculateAutocorrelations(){
		Vector3D[] workData = new Vector3D[observations.size()];
		workData = observations.toArray(workData);
		double[] autocorrelations = strategy
				.getAutocorrelations(workData);
		return autocorrelations;
	}
		
}
