package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.List;

import com.tcb.mdAnalysis.statistics.StandardStatistics;

public abstract class AbstractAutocorrelationStrategy<T> implements AutocorrelationStrategy<T> {
		
	protected void normalize(double[] arr){
		double norm = arr[0];
		for(int i=0; i<arr.length;i++){
			arr[i] /= norm;
		}
	}
	
}
