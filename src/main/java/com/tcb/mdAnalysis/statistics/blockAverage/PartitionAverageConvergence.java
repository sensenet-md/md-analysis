package com.tcb.mdAnalysis.statistics.blockAverage;

import java.util.List;
import java.util.stream.Collectors;

import com.tcb.mdAnalysis.exceptions.NotConvergedException;
import com.tcb.common.util.ListPartitioner;
import com.tcb.common.util.Tuple;

public class PartitionAverageConvergence {
	private List<Double> values;
	
	public PartitionAverageConvergence(List<Double> values){
		this.values = values;
	}
	
	public Tuple<Double,Double> getConverged(int windowLength, double convergenceLimit) throws NotConvergedException {
		List<List<Double>> xyPartitions = ListPartitioner.equal_partition(values, windowLength);
		for(int i = 0;i < xyPartitions.size()-1;i++){
			double partitionAverage = xyPartitions.get(i).stream()
					.collect(Collectors.averagingDouble(t -> t));
			double nextPartitionAverage = xyPartitions.get(i+1).stream()
					.collect(Collectors.averagingDouble(t -> t));
			double difference = partitionAverage - nextPartitionAverage;
			if(Math.abs(difference) <= convergenceLimit) 
				return new Tuple<Double,Double>(
						(i * windowLength) + 0.5 * windowLength,
						partitionAverage);			
			
		}
		throw new NotConvergedException();
	}
	
}
