package com.tcb.mdAnalysis.statistics.blockAverage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.common.util.ListPartitioner;
import com.tcb.common.util.Tuple;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class BlockAverage {
	private List<Double> observations;
	
	public BlockAverage(List<Double> observations){
		this.observations = observations;
	}
		
	public Tuple<Double,Double> getConvergedError(int convergenceWindowLength, double convergenceLimit){
		List<Double> blockErrors = getBlockedStandardErrors();
		PartitionAverageConvergence converge = new PartitionAverageConvergence(blockErrors);
		Tuple<Double,Double> converged = converge.getConverged(convergenceWindowLength,convergenceLimit);
		return converged;
	}
		
	public List<Double> getBlockedStandardErrors(){
		int maxBlockSize = observations.size() / 4;
		List<Double> blockErrors = new ArrayList<Double>();
		for(int blockSize=1;blockSize <= maxBlockSize;blockSize++){
			blockErrors.add(getBlockedStandardError(blockSize));
		}
		return blockErrors;
	}
	
	private Double getBlockedStandardError(int blockSize){
		List<List<Double>> blocks = 
				ListPartitioner.equal_partition(observations, blockSize);
		List<Double> blocksAverages = blocks.stream()
				.map(b -> new StandardStatistics(b))
				.map(s -> s.getMean())
				.collect(Collectors.toList());
		StandardStatistics averagesStatistics = new StandardStatistics(blocksAverages);
		return averagesStatistics.standardError();
	}
	
}
