package com.tcb.mdAnalysis.statistics;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import java.lang.Math;
import java.util.List;
import java.util.stream.DoubleStream;

public class StandardStatistics extends SummaryStatistics {
	private static final long serialVersionUID = 5771790152479218155L;
	
	public StandardStatistics(List<Double> values){
		super();
		values.forEach(v -> this.addValue(v));
	}
	
	public StandardStatistics(double[] values){
		super();
		for(int i=0;i<values.length;i++){
			this.addValue(values[i]);
		}
	}
	
	public StandardStatistics(float[] values){
		super();
		for(int i=0;i<values.length;i++){
			this.addValue(values[i]);
		}
	}
	
	public StandardStatistics(Double[] values){
		super();
		for(int i=0;i<values.length;i++){
			this.addValue(values[i]);
		}
	}
	
	public StandardStatistics(Float[] values){
		super();
		for(int i=0;i<values.length;i++){
			this.addValue(values[i]);
		}
	}
		
	public Double standardError(){
		return standardError(this.getN());
	}
	
	public Double standardError(double N){
		double stdErr = this.getStandardDeviation() / Math.sqrt(N);
		return stdErr;
	}
	
}
