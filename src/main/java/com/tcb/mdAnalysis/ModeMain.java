package com.tcb.mdAnalysis;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.mdAnalysis.cli.Mode;

public abstract class ModeMain {
	
	protected abstract Mode getMode();
	
	protected static CommandLine parseArguments(Options options, String[] args, String modeName) throws ParseException{
		try{
			CommandLine cmd = new DefaultParser().parse(options, args);
			if(cmd.hasOption('h')){
				printHelp(options,modeName);
				System.exit(1);
			}
			return cmd;
		} catch(ParseException e){
			System.out.println(e.getMessage());
			printHelp(options,modeName);
			System.exit(1);
			return null;
		}
	}
	
	protected static void printHelp(Options options, String modeName){
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(
				String.format("${BIN} %s", modeName),
				options, true);
	}
}
