package com.tcb.mdAnalysis.math.integration;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.mdAnalysis.math.integration.ExponentialDecayIntegrator;
import com.tcb.mdAnalysis.math.integration.Integrator;

public class ExponentialDecayIntegratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testIntegrateOne() {
		Integrator integrator = new ExponentialDecayIntegrator(2,4);
		Double integrated = integrator.integrate();
		
		assertEquals((Double) 0.5,integrated);
	}
	
	@Test
	public void testIntegrateTwo() {
		Integrator integrator = new ExponentialDecayIntegrator(1,10);
		Double integrated = integrator.integrate();
		
		assertEquals((Double) 0.1,integrated);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void shouldFailForIntInt() {
		Integrator integrator = new ExponentialDecayIntegrator(2,4);
		Double integrated = integrator.integrate(0,1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldFailIfBeq0() {
		Integrator integrator = new ExponentialDecayIntegrator(2,0);
		Double integrated = integrator.integrate();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldFailIfB_lt_0() {
		Integrator integrator = new ExponentialDecayIntegrator(2,-1);
		Double integrated = integrator.integrate();
	}
	
	@Test
	public void shouldSuceedIfA_eq_0() {
		Integrator integrator = new ExponentialDecayIntegrator(0,5);
		Double integrated = integrator.integrate();
		
		assertEquals((Double) 0., integrated);
	}
	
	@Test
	public void shouldSuceedIfA_lt_0() {
		Integrator integrator = new ExponentialDecayIntegrator(-1,5);
		Double integrated = integrator.integrate();
		
		assertEquals( (Double) (-0.2), integrated);
	}
	
	

}
