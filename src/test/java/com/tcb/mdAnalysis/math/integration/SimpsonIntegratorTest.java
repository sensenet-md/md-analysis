package com.tcb.mdAnalysis.math.integration;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.math.integration.SimpsonIntegrator;

public class SimpsonIntegratorTest {

	private List<Double> observations;
	private SimpsonIntegrator integrator;
	private Double refArea;

	@Before
	public void setUp() throws Exception {
		this.observations = Arrays.asList(0,1,2,3,4,5,6).stream()
				.map(i -> i.doubleValue())
				.collect(Collectors.toList());
		this.refArea = 18.0d;
		this.integrator = new SimpsonIntegrator(observations);
	}

	@Test
	public void testIntegrate() {
		Double area = integrator.integrate();
		assertEquals(refArea,area,0.001);
	}

}
