package com.tcb.mdAnalysis.math.integration;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedExponentialRegression;

public class SCF_ExponentialDecayIntegratorTest {

	private List<Double> data;
	private double refB;
	private SimpsonIntegrator integrator;
	private int integrationTime;
	private SCF_ExponentialDecayIntegrator testIntegrator;

	@Before
	public void setUp() throws Exception {
		this.data = Arrays.asList(
				1.0, 0.78, 0.61, 0.47, 0.37, 0.29, 0.22, 0.17, 0.14, 0.11,
				0.08, 0.06, 0.05, 0.04, 0.03, 0.02, 0.02, 0.01, 0.01, 0.01);
		this.refB = 0.25;
		this.integrator = new SimpsonIntegrator(data);
		this.integrationTime = 10;
		this.testIntegrator = new SCF_ExponentialDecayIntegrator(integrator, integrationTime);
	}
	
	@Test
	public void testIntegrate() {
		Double integrated = testIntegrator.integrate();
		Regression reg = new WeightedExponentialRegression(data);
		Double refIntegrated = new ExponentialDecayIntegrator(reg.getA(), -reg.getB()).integrate();
		System.out.println("---");
		System.out.println(integrated);
		System.out.println(refIntegrated);
	}
	
}
