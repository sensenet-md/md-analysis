package com.tcb.mdAnalysis.math;

import static com.tcb.mdAnalysis.math.PowerOf2.isPowerOf2;
import static com.tcb.mdAnalysis.math.PowerOf2.nextPowerOf2;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PowerOf2Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testNextPowerOf2() {
		assertEquals(1,nextPowerOf2(1));
		assertEquals(2,nextPowerOf2(2));
		assertEquals(4,nextPowerOf2(4));
		assertEquals(8,nextPowerOf2(5));
		assertEquals(1024,nextPowerOf2(1000));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNextPowerOf2WithNull(){
		nextPowerOf2(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNextPowerOf2WithNegative(){
		nextPowerOf2(-1);
	}
	
	@Test
	public void testIsPowerOf2(){
		assertTrue(isPowerOf2(1));
		assertTrue(isPowerOf2(2));
		assertTrue(isPowerOf2(1024));
		assertFalse(isPowerOf2(3));
		assertFalse(isPowerOf2(1025));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIsPowerOf2WithNull(){
		isPowerOf2(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIsPowerOf2WithNegative(){
		isPowerOf2(-1);
	}

}
