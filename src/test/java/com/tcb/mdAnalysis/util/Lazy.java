package com.tcb.mdAnalysis.util;

import java.util.function.Supplier;

/*
 * Source: https://stackoverflow.com/questions/29132884/lazy-field-initialization-with-lambdas
 */

interface Lazy<T> extends Supplier<T> {
	Supplier<T> init();
    public default T get() { return init().get(); }
    
    static <U> Supplier<U> lazily(Lazy<U> lazy) { return lazy; }
    static <T> Supplier<T> value(T value) { return ()->value; }
};


