package com.tcb.mdAnalysis.statistics.error.blockAverage;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.exceptions.NotConvergedException;
import com.tcb.mdAnalysis.statistics.blockAverage.PartitionAverageConvergence;
import com.tcb.common.util.Tuple;

public class PartitionAverageConvergenceTest {
	private List<Double> values;
	private int convergenceWindowSize;
	private PartitionAverageConvergence convergence;

	@Before
	public void setUp() throws Exception {
		this.values = Arrays.asList(10.,20., // avg 15
				30.,40., // avg 35
				45.,50., // avg 47.5
				51.,49., // avg 50
				52.,50.); // avg 51
		this.convergenceWindowSize = 2;
		this.convergence = new PartitionAverageConvergence(this.values);
	}

	@Test
	public void testGetConvergenceIndexLimit1() {
		double convergenceLimit = 1.;
		Tuple<Double,Double> converged = convergence.getConverged(convergenceWindowSize,convergenceLimit);
		
		assertEquals(7., converged.one(),0.01);
		assertEquals(50,converged.two(),0.01);
	}
	
	@Test
	public void testGetConvergenceIndexLimit13() {
		double convergenceLimit = 13.;
		Tuple<Double,Double> converged = convergence.getConverged(convergenceWindowSize,convergenceLimit);
		
		assertEquals(3., converged.one(),0.01);
		assertEquals(35.,converged.two(),0.01);
	}
	
	@Test
	public void testGetConvergenceIndexLimit100() {
		double convergenceLimit = 100;
		Tuple<Double,Double> converged = convergence.getConverged(convergenceWindowSize,convergenceLimit);
		
		assertEquals(1., converged.one(),0.01);
		assertEquals(15.0,converged.two(),0.01);
	}
	
	@Test(expected=NotConvergedException.class)
	public void testGetConvergenceIndexLimitTooLow() {
		double convergenceLimit = 0.01;
		Tuple<Double,Double> converged = convergence.getConverged(convergenceWindowSize,convergenceLimit);
	}
	
	
	
	
	

}
