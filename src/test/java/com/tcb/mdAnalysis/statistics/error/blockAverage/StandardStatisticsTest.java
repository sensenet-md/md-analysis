package com.tcb.mdAnalysis.statistics.error.blockAverage;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import com.tcb.mdAnalysis.statistics.StandardStatistics;
import org.junit.Before;
import org.junit.Test;

public class StandardStatisticsTest {
	private StandardStatistics stat;
	private List<Double> values;

	@Before
	public void setUp() throws Exception {
		this.values = Arrays.asList(0.0,2.0,4.0);
		this.stat = new StandardStatistics(this.values);
	}

	@Test
	public void testStandardStatistics() {
		assertEquals(3,this.stat.getN());
	}

	@Test
	public void testStandardError() {
		assertEquals(1.1547d,(double) this.stat.standardError(),0.0001);
	}

}
