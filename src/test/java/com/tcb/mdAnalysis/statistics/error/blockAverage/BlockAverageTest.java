package com.tcb.mdAnalysis.statistics.error.blockAverage;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.csv.CSV;
import com.tcb.csv.CSV_Reader;
import com.tcb.mdAnalysis.statistics.blockAverage.BlockAverage;
import com.tcb.mdAnalysis.util.PathUtil;

public class BlockAverageTest {
	
	private List<Double> values;
	private BlockAverage blockAverage;
	private CSV csv;

	@Before
	public void setUp() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		String baseResourcePath = "exampleData/";
		Path path = PathUtil.
				fromResource(classLoader, baseResourcePath + "temperature.csv");
		this.csv = new CSV_Reader(path.toString(),",",true).getCSV();
		this.values = csv.getColumns().get(1).stream()
				.map(s -> Double.valueOf(s))
				.collect(Collectors.toList());
		this.blockAverage = new BlockAverage(this.values);
	}

	@Ignore
	@Test
	public void testGetConvergedError() {
		// TODO Implement test
	}

	@Test
	public void testGetBlockedStandardErrors() {
		// reference values: program from https://gist.github.com/shane5ul/6289436ace018e6e9e79ee3489be2b8f
		List<Double> ref = Arrays.asList(
			0.2240,0.2890,
			0.3339,0.3701,
			0.4030,0.4346,
			0.4577,0.4762,
			0.5070,0.5197,
			0.5466,0.5727);
		List<Double> test = blockAverage.getBlockedStandardErrors();
		for(int i=0;i<ref.size();i++){
			assertEquals(ref.get(i),test.get(i),0.01);
		}
	}

}
