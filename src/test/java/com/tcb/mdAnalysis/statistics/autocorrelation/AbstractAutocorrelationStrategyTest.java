package com.tcb.mdAnalysis.statistics.autocorrelation;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.blockAverage.BlockAverage;

public abstract class AbstractAutocorrelationStrategyTest {
	
	protected Double[] values;
	protected AutocorrelationStrategy<Double> strategy;
	protected double[] refAutocorrelations;

	@Before
	public void setUp() throws Exception {
		this.values = IntStream.of(
				1,1,0,0,1,1,0,0,0,0,1,1,1,1,0,0)
				.mapToDouble(i -> (double)i)
				.boxed()
				.toArray(Double[]::new);
		// Source refValues: R 2.11.1 acf() function
		this.refAutocorrelations = new double[] {
				1.000,0.625,0.25,
				0.25,0.25,0.25,0.25,0.25

		};
		this.strategy = this.getAutocorrelationStrategy();
	}

	protected abstract AutocorrelationStrategy<Double> getAutocorrelationStrategy();

	@Test
	public void testGetAutocorrelations() {
		double[] autoCorrelations = strategy.getAutocorrelations(values);
		for(int i=0;i<refAutocorrelations.length;i++){
			assertEquals(refAutocorrelations[i],autoCorrelations[i],0.001);
		}
	}
	
}
