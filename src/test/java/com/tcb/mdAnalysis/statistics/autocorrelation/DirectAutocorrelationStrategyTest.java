package com.tcb.mdAnalysis.statistics.autocorrelation;

import com.tcb.mdAnalysis.math.DoubleUtil;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.DirectAutocorrelationStrategy;

public class DirectAutocorrelationStrategyTest extends AbstractAutocorrelationStrategyTest {

	@Override
	protected AutocorrelationStrategy<Double> getAutocorrelationStrategy() {
		return new DirectAutocorrelationStrategy<Double>(DoubleUtil::product);
	}
	
	protected Double multiplier(Double a, Double b){
		return a*b;
	}

}
