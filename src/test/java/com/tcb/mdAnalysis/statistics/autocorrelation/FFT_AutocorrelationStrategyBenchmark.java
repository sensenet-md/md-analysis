package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.FFT_AutocorrelationStrategy;
import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorImpl;

public class FFT_AutocorrelationStrategyBenchmark extends AbstractAutocorrelationBenchmark {

	@Override
	protected AutocorrelationStrategy<Double> getAutocorrelationStrategy() {
		return new FFT_AutocorrelationStrategy();
	}

}
