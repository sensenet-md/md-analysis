package com.tcb.mdAnalysis.statistics.autocorrelation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.lifetime.Lifetime;

public class VectorAutocorrelationTest {
	
	
	private List<Vector3D> values;
	private Autocorrelation autocorrelation;
	private int max_h;
	

	@Before
	public void setUp(){
		this.values = Arrays.asList(
				new Vector3D(0.0094,  -0.0145,  -0.9999),
				new Vector3D(-0.1193,   0.2017,  -0.9722),
				new Vector3D(-0.0689,  -0.0246,  -0.9973),
				new Vector3D(0.0326,  -0.0346,  -0.9989),
				new Vector3D(-0.0076,  -0.0456,  -0.9989)
				);
		this.max_h = 3;
		this.autocorrelation = new VectorAutocorrelation(values, max_h);
	}
		
	@Test
	public void testGetAutocorrelations(){
		// TODO: Check reference value!
		double[] autocorrelations = {1,0.78,0.59,0.396};
		List<Double> testAutocorrelations = autocorrelation.getAutocorrelations();
		for(int i=0;i<autocorrelations.length;i++){
			assertEquals(autocorrelations[i],testAutocorrelations.get(i),0.01);
		}
		
	}
	
	
}

