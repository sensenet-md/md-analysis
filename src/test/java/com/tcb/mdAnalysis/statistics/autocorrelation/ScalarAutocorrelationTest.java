package com.tcb.mdAnalysis.statistics.autocorrelation;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.StandardStatistics;
import com.tcb.mdAnalysis.statistics.autocorrelation.ScalarAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.lifetime.Lifetime;

public class ScalarAutocorrelationTest {
	
	
	private List<Double> values;
	private Autocorrelation autocorrelation;
	private int max_h;

	@Before
	public void setUp(){
		this.values = IntStream.of(
				1,1,0,0,1,1,0,0,0,0,1,1,1,1,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
				.mapToDouble(i -> (double)i)
				.boxed()
				.collect(Collectors.toList());
		this.max_h = 6;
		this.autocorrelation = new ScalarAutocorrelation(values, max_h);
	}
		
	@Test
	public void testGetAutocorrelations(){
		// TODO: Check reference value!
		double[] autocorrelations = {1,0.53,0.0625,0.052,0.041,0.072,0.1};
		List<Double> testAutocorrelations = autocorrelation.getAutocorrelations();
		for(int i=0;i<autocorrelations.length;i++){
			assertEquals(autocorrelations[i],testAutocorrelations.get(i),0.01);
		}
		
	}
	
	
}

