package com.tcb.mdAnalysis.statistics.autocorrelation.fluoresceneAnisotropy;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.cli.VectorInputData;
import com.tcb.mdAnalysis.cli.XYInputData;
import com.tcb.mdAnalysis.math.LegendrePolynomials;
import com.tcb.mdAnalysis.math.vector.VectorUtil;
import com.tcb.mdAnalysis.statistics.autocorrelation.Autocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.UnbiasedEstimateDirectAutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.VectorAutocorrelation;
import com.tcb.mdAnalysis.statistics.autocorrelation.fluorescenceAnisotropy.FluorescenceAnisotropy;
import com.tcb.mdAnalysis.util.PathUtil;

public class FluorescenceAnisotropyTest {
	
	private Autocorrelation autocorrelation;
	private List<Double> refAnisotropies;
	private int max_h;
	
	@Before
	public void setUp() throws URISyntaxException, IOException{
		ClassLoader classLoader = getClass().getClassLoader();
		String baseResourcePath = "autocorrelation/FluorescenceAnisotropyTest/";
		Path orientationVectorsPath = PathUtil.fromResource(classLoader, baseResourcePath + "orientations.cpptraj.xvg");
		Path refAnisotropiesPath = PathUtil.fromResource(classLoader, baseResourcePath + "fluorescence-anisotropy.cpptraj.xvg");
		List<Vector3D> orientationVectors = new VectorInputData(orientationVectorsPath.toString(), 1).vectors;
		// Reference anisotropies source: Calculated from AmberTools16 cpptraj timecorr
		this.refAnisotropies = new XYInputData(refAnisotropiesPath.toString(), "#Time", "<P2>").Y;
		this.max_h = 1000;
		AutocorrelationStrategy<Vector3D> strategy = new UnbiasedEstimateDirectAutocorrelationStrategy<Vector3D>(max_h, VectorUtil::dotProduct,
				LegendrePolynomials::legendre2);
		this.autocorrelation = new FluorescenceAnisotropy(orientationVectors, max_h, strategy);
	}
		
	@Test
	public void testGetAutocorrelations(){
		List<Double> testAutocorrelations = autocorrelation.getAutocorrelations();
		for(int i=0;i<max_h;i++){
			assertEquals(refAnisotropies.get(i),testAutocorrelations.get(i),0.001);
		}
		
	}
	
	@Test
	public void testGetInputDataSize() {
		assertEquals(8000, (int)autocorrelation.getInputDataSize());
	}

}
