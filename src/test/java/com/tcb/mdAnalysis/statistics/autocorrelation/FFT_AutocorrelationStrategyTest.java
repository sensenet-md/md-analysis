package com.tcb.mdAnalysis.statistics.autocorrelation;

import static org.junit.Assert.*;

import org.junit.Test;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.mdAnalysis.statistics.autocorrelation.FFT_AutocorrelationStrategy;

public class FFT_AutocorrelationStrategyTest extends AbstractAutocorrelationStrategyTest {

	@Override
	protected AutocorrelationStrategy getAutocorrelationStrategy() {
		return new FFT_AutocorrelationStrategy();
	}
			
}
