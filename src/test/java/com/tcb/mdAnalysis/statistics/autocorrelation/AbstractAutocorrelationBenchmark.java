package com.tcb.mdAnalysis.statistics.autocorrelation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationStrategy;
import com.tcb.monitor.Monitor;
import com.tcb.monitor.MonitorImpl;
import com.tcb.monitor.MonitorStorage;

public abstract class AbstractAutocorrelationBenchmark {

	protected List<Double[]> data;
	protected MonitorImpl monitor1;
	protected MonitorImpl monitor2;
	protected int dataLength;
	protected int max_h;

	@Before
	public void setUp(){
		Random r = new Random();
		this.dataLength = 10000;
		this.data = IntStream.range(0, 100)
				.boxed()
				.map(i -> r.doubles(dataLength).boxed()
						.toArray(Double[]::new))
				.collect(Collectors.toList());
		this.max_h = dataLength - 1;
		this.monitor1 = new MonitorImpl("test1");
		this.monitor2 = new MonitorImpl("test2");
	}
	
	@After
	public void tearDown(){
		System.out.println(MonitorStorage.getString());
		MonitorStorage.reset();
	}
	
	@Test
	public void testGetAutocorrelations(){
		List<double[]> results = new ArrayList<double[]>();
		for(Double[] d:data){
			monitor1.start();
			AutocorrelationStrategy<Double> strategy = getAutocorrelationStrategy();
			monitor1.stop();
			monitor2.start();
			results.add(strategy.getAutocorrelations(d));
			monitor2.stop();
		}
	}
	
	protected abstract AutocorrelationStrategy<Double> getAutocorrelationStrategy();
}
