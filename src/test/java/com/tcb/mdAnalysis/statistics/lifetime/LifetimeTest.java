package com.tcb.mdAnalysis.statistics.lifetime;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.autocorrelation.AutocorrelationAnalysis;
import com.tcb.mdAnalysis.statistics.autocorrelation.FFT_AutocorrelationStrategy;

public class LifetimeTest {

	
	private List<Double> values;
	private Lifetime lifetime;
	private AutocorrelationAnalysis analysis;
	private int max_h;

	@Before
	public void setUp(){
		this.values = IntStream.of(
				1,1,0,0,1,1,0,0,0,0,1,1,1,1,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
				.mapToDouble(i -> (double)i)
				.boxed()
				.collect(Collectors.toList());
		this.max_h = 6;
		this.lifetime = new Lifetime(values, max_h, new FFT_AutocorrelationStrategy());
		this.analysis = new AutocorrelationAnalysis(lifetime.getAutocorrelations(), 0.1);
	}
	
	@Ignore
	@Test
	public void testGetAutocorrelationTime(){
		// TODO: Check reference value!
		assertEquals(2.85,analysis.getAutocorrelationTime(),0.01);
	}
	
	@Ignore
	@Test
	public void testGetEffectiveSampleSize(){
		// TODO: Check reference value!
		assertEquals(11.21,analysis.getEffectiveSampleSize(lifetime.getInputDataSize()),0.01);
	}
	
	@Ignore
	@Test
	public void testGetAutocorrelations(){
		// TODO: Check reference value!
		double[] autocorrelations = {1,0.625,0.25,0.25,0.25,0.25,0.25};
		List<Double> testAutocorrelations = lifetime.getAutocorrelations();
		for(int i=0;i<autocorrelations.length;i++){
			assertEquals(autocorrelations[i],testAutocorrelations.get(i),0.01);
		}
		
	}
}
