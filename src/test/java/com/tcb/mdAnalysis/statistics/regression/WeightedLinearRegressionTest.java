package com.tcb.mdAnalysis.statistics.regression;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedLinearRegression;

public class WeightedLinearRegressionTest {
	protected List<Double> observations;
	protected Double refA;
	protected Double refB;
	protected Regression testRegression;
	protected Regression testRegressionUnitWeights;
	protected Regression testRegressionChangedWeights;
	protected Double refAchangedWeights;
	protected Double refBchangedWeights;
	protected Double refPredict;
	protected List<Double> refRegressionLine;

	@Before
	public void setUp() throws Exception {
		this.observations = Arrays.asList(0.0,5.,40.);
		this.refA = -5.0; // checked with R 2.11.1
		this.refB = 20.0; // checked with R 2.11.1
		this.refAchangedWeights = -3.6; // checked with R 2.11.1
		this.refBchangedWeights = 15.8; // checked with R 2.11.1
		this.refPredict = 5.0;
		this.refRegressionLine = Arrays.asList(-5.0,15.0,35.0);
		this.testRegression = new WeightedLinearRegression(observations);
		this.testRegressionUnitWeights = new WeightedLinearRegression(observations,Arrays.asList(1.,1.,1.));
		this.testRegressionChangedWeights = new WeightedLinearRegression(observations,Arrays.asList(1.,1.,0.3));
	}

	@Test
	public void testGetA() {
		assertEquals(refA,testRegression.getA(),0.01);
	}
	
	@Test
	public void testGetAwithUnitWeights() {
		
		assertEquals(refA,testRegressionUnitWeights.getA(),0.01);
	}
	
	@Test
	public void testGetAwithWeights() {
		assertEquals(refAchangedWeights,testRegressionChangedWeights.getA(),0.01);
	}

	@Test
	public void testGetB() {
		
		assertEquals(refB,testRegression.getB(),0.01);
	}
	
	@Test
	public void testGetBwithUnitWeights() {
		
		assertEquals(refB,testRegressionUnitWeights.getB(),0.01);
	}
	
	@Test
	public void testGetBwithWeights() {

		assertEquals(refBchangedWeights,testRegressionChangedWeights.getB(),0.01);
	}

	@Test
	public void testPredict() {
		
		assertEquals(refPredict,testRegression.predict(.5),0.01);
	}

	@Test
	public void testRegressionLine() {
		
		List<Double> testRegressionLine = testRegression.regressionLine(0, 3);
		for(int i=0;i<refRegressionLine.size();i++){
			assertEquals(refRegressionLine.get(i),testRegressionLine.get(i),0.01);
		}
		
		
	}
	
	@Test
	public void testGetRoots() {
		List<Double> roots = testRegression.getRoots(0.);
		assertEquals(1, roots.size());
		assertEquals(0.25, roots.get(0), 0.01);
		
		roots = testRegression.getRoots(5.);
		assertEquals(1, roots.size());
		assertEquals(0.5, roots.get(0), 0.01);
	
		roots = testRegressionChangedWeights.getRoots(0.);
		assertEquals(1, roots.size());
		assertEquals(0.2278, roots.get(0), 1e-4);
		
		roots = testRegressionChangedWeights.getRoots(-2.);
		assertEquals(1, roots.size());
		assertEquals(0.1012, roots.get(0), 1e-4);
	}

}
