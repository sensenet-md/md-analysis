package com.tcb.mdAnalysis.statistics.regression;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.mdAnalysis.statistics.regression.Regression;
import com.tcb.mdAnalysis.statistics.regression.WeightedExponentialRegression;
import com.tcb.mdAnalysis.statistics.regression.WeightedLinearRegression;

public class WeightedExponentialRegressionTest extends WeightedLinearRegressionTest {
	
	// TODO Improve test values

	@Before
	public void setUp() throws Exception {
		this.observations = Arrays.asList(1.0,2.5,8.0);
		this.refA = 0.96; // checked with R 2.11.1
		this.refB = 1.04; // checked with R 2.11.1
		this.refAchangedWeights = 0.97; // checked with R 2.11.1
		this.refBchangedWeights = 1.00; // checked with R 2.11.1
		this.refPredict = 1.61;
		this.refRegressionLine = Arrays.asList(0.96,2.71,7.67);
		this.testRegression = new WeightedExponentialRegression(observations);
		this.testRegressionUnitWeights = new WeightedExponentialRegression(observations,Arrays.asList(1.,1.,1.));
		this.testRegressionChangedWeights = new WeightedExponentialRegression(observations,Arrays.asList(1.,1.,0.3));
	}
	
	@Test
	public void testGetRoots() {
		Regression reg = new WeightedExponentialRegression(Arrays.asList(8.,2.5,1.));
				
		List<Double> roots = reg.getRoots(1.);
		assertEquals(1, roots.size());
		assertEquals(1.96, roots.get(0), 0.01);  // checked with scipy.optimize.root_scalar v1.4.1
		
		roots = reg.getRoots(0.1);
		assertEquals(1, roots.size());
		assertEquals(4.17, roots.get(0), 0.01); // checked with scipy.optimize.root_scalar v1.4.1

	
	}
	
}
